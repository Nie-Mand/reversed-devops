import json


# ADD, COPY, WORKDIR

def extract_copy(command):
    splitted = command.split("COPY")
    if len(splitted) == 1:
        return False
    if command.endswith(" / "):
        return False
    
    return True

def extract_add(command):
    splitted = command.split("ADD")
    if len(splitted) == 1:
        return False
    if command.endswith(" / "):
        return False
    return True

def extract_workdir(command):
    splitted = command.split("WORKDIR")
    if len(splitted) == 1:
        return False
    return True

def load_layers(history_file):
    layers = []
    workdir = ""
    hf = open(history_file, 'r').read()
    hf = json.loads(hf)
    for layer in hf['layer']:
        sha = layer['id']
        command = layer["command"]
        if extract_copy(command):
            layers.append(sha + " " + workdir)
        if extract_add(command):
            layers.append(sha + " " + workdir)
        if extract_workdir(command):
            path = command.split("WORKDIR")[-1].strip()
            workdir = path

    open('layers.txt', 'w').write('\n'.join(layers) + '\n')

load_layers('fein.json')