IMAGE_FULL_NAME=niemandx/fein:latest
IMAGE_NAME=fein

# extract: clean-input clean-output
# 	@dive $(IMAGE_FULL_NAME) -j $(IMAGE_NAME).json
# 	@python3 extract.py
# 	@mkdir -p tmp
# 	@sudo docker save $(IMAGE_NAME) -o tmp/$(IMAGE_NAME).tar
# 	@sudo tar -xf tmp/$(IMAGE_NAME).tar -C tmp
# 	@mkdir -p out
# 	@cat layers.txt | while read -r layer; do \
# 		HASH=`echo $$layer | cut -d' ' -f1`; \
# 		WORKD=`echo $$layer | cut -d' ' -f2`; \
# 		sudo tar -xf tmp/$$HASH/layer.tar -C tmp/$$HASH; \
# 		sudo rm -rf tmp/$$HASH/layer.tar tmp/$$HASH/VERSION tmp/$$HASH/json; \
# 		sudo cp -r tmp/$$HASH$$WORKD/* out; \
# 		done
# 	@make clean-input

# clean-input:
# 	@sudo rm -rf tmp
# 	@sudo rm -f $(IMAGE_NAME).json
# 	@sudo rm -rf layers.txt

# clean-output:
# 	@sudo rm -rf out




extract: clean-input clean-output
	@dive $(IMAGE_FULL_NAME) -j $(IMAGE_NAME).json
	@python3 extract.py
	@mkdir -p tmp
	@docker save $(IMAGE_NAME) -o tmp/$(IMAGE_NAME).tar
	@tar -xf tmp/$(IMAGE_NAME).tar -C tmp
	@mkdir -p out
	@cat layers.txt | while read -r layer; do \
		HASH=`echo $$layer | cut -d' ' -f1`; \
		WORKD=`echo $$layer | cut -d' ' -f2`; \
		tar -xf tmp/$$HASH/layer.tar -C tmp/$$HASH; \
		rm -rf tmp/$$HASH/layer.tar tmp/$$HASH/VERSION tmp/$$HASH/json; \
		cp -r tmp/$$HASH$$WORKD/* out; \
		done
	@make clean-input

clean-input:
	@rm -rf tmp
	@rm -f $(IMAGE_NAME).json
	@rm -rf layers.txt

clean-output:
	@rm -rf out

@PHONY: extract clean clean-input clean-output